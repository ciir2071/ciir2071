package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {

    @Before
    public void setup() {
        //
    }

    @Test
    public void TC1_ECP() {
        CartiRepo repo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
        Carte carte = new Carte();
        carte.setTitlu("Amintiri");
        carte.adaugaReferent("Ion Creanga");
        carte.setAnAparitie("1860");
        carte.adaugaCuvantCheie("furat");
        carte.adaugaCuvantCheie("copii");

        try {
            ctrl.adaugaCarte(carte);
            assertTrue(true);
        }
        catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void TC2_ECP() {
        CartiRepo repo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
        Carte carte = new Carte();
        carte.setTitlu("");
        carte.adaugaReferent("Armando Sanchez");
        carte.setAnAparitie("1999");
        carte.adaugaCuvantCheie("drama");
        carte.adaugaCuvantCheie("iubire");

        try {
            ctrl.adaugaCarte(carte);
            fail();
        }
        catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void TC3_ECP() {
        CartiRepo repo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
        Carte carte = new Carte();
        carte.setTitlu("Muntele");
        carte.adaugaReferent("Ioan");
        carte.setAnAparitie("1399");
        carte.adaugaCuvantCheie("munte");
        carte.adaugaCuvantCheie("frig");

        try {
            ctrl.adaugaCarte(carte);
            fail();
        }
        catch (Exception e) {
            //e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void TC1_BVA() {
        CartiRepo repo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
        Carte carte = new Carte();
        carte.setTitlu("");
        carte.adaugaReferent("Ioan");
        carte.setAnAparitie("1989");
        carte.adaugaCuvantCheie("aventura");
        carte.adaugaCuvantCheie("mare");

        try {
            ctrl.adaugaCarte(carte);
            fail();
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void TC2_BVA() {
        CartiRepo repo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
        Carte carte = new Carte();
        //carte.setTitlu("");
        carte.adaugaReferent("Ionescu");
        carte.setAnAparitie("2013");
        carte.adaugaCuvantCheie("SF");
        carte.adaugaCuvantCheie("fructe");

        try {
            ctrl.adaugaCarte(carte);
            fail();
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void TC3_BVA() {
        CartiRepo repo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
        Carte carte = new Carte();
        carte.setTitlu("M");
        carte.adaugaReferent("Janos");
        carte.setAnAparitie("1860");
        carte.adaugaCuvantCheie("mate");
        carte.adaugaCuvantCheie("geometrie");

        try {
            ctrl.adaugaCarte(carte);
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void TC4_BVA() {
        CartiRepo repo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
        Carte carte = new Carte();
        carte.setTitlu("Manastur2");
        carte.adaugaReferent("Andreeea");
        carte.adaugaReferent("Nelson");
        carte.setAnAparitie("2019");
        carte.adaugaCuvantCheie("maaai");
        carte.adaugaCuvantCheie("aloo");
        carte.adaugaCuvantCheie("dar");
        carte.adaugaCuvantCheie("nu");


        try {
            ctrl.adaugaCarte(carte);
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}