import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;


public class TestF03 {

    private static CartiRepoInterface cartiRepo;
    private static BibliotecaCtrl controller;

    @Before
    public void initializer() {
        cartiRepo = new CartiRepoMock(); //mock?
        controller = new BibliotecaCtrl(cartiRepo);

        try {
            controller.adaugaCarte(Carte.getCarteFromString("Muntele;Marius;1966;EFN;povesti"));
            controller.adaugaCarte(Carte.getCarteFromString("Cartea;Cristi;1988;Pedagogica;litere"));
            controller.adaugaCarte(Carte.getCarteFromString("Gratare;Gabriel;1966;EFN;poezii"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test_valid() throws Exception {
        List<Carte> carti = controller.getCartiOrdonateDinAnul("1966");
        //for (Carte carte : carti)
        //    System.out.println(carti.toString());

        assertEquals(carti.size(), 2);
        assertEquals(carti.get(0).getCuvinteCheie().get(0), "poezii");
    }

    @Test(expected = Exception.class)
    public void test_invalid() throws Exception {
        assertTrue(controller.cautaCarte("").size() == 0);
    }

}
