import biblioteca.control.BibliotecaCtrl;
import biblioteca.control.BibliotecaCtrlTest;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import biblioteca.view.Consola;
import org.junit.Before;
import org.junit.Test;
import repository.repo.CartiRepoTest;

import java.util.Collections;

import static junit.framework.TestCase.assertEquals;

public class IntegrationTest {

    private static CartiRepoInterface repo;
    private static BibliotecaCtrl controller;
    //private static Consola consola;

    @Before
    public void initializer() {
        repo = new CartiRepoMock();
        controller = new BibliotecaCtrl(repo);
        //consola = new Consola(controller);
    }

    @Test
    public void Test_A() throws Exception {
        (new BibliotecaCtrlTest()).TC1_BVA();
    }

    @Test
    public void Test_B() throws Exception {
        CartiRepoTest test = new CartiRepoTest();
        test.initializer();
        test.test_1();
    }

    @Test
    public void Test_C() throws Exception {
        TestF03 test = new TestF03();
        test.initializer();
        test.test_valid();
    }

    private void adaugareCarte() {
        Carte carte = new Carte();
        carte.setAnAparitie("1998");
        carte.setTitlu("Acasa");
        carte.setReferenti(Collections.singletonList("referent"));
        carte.setCuvinteCheie(Collections.singletonList("cuv"));
        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cautareDupaAutor() {
        try {
            for (Carte carte : controller.cautaCarte("Ceausescu"))
                System.out.println(carte.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cautareDupaAn() {
        try {
            for (Carte carte : controller.getCartiOrdonateDinAnul("1990"))
                System.out.println(carte.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void BigBang() throws Exception { // corect?

        adaugareCarte();
        cautareDupaAutor();
        cautareDupaAn();
        //
        assertEquals(controller.getCarti().size(), 1);
        assertEquals(controller.cautaCarte("Ceausescu").size(), 0);
        assertEquals(controller.getCartiOrdonateDinAnul("1998").size(), 1);

    }

    @Test
    public void TopDown_A() throws Exception {
        adaugareCarte();
        assertEquals(controller.getCarti().size(), 1);

        //cautareDupaAutor();
        //assertEquals(controller.cautaCarte("Ceausescu").size(), 0);

        //cautareDupaAn();
        //assertEquals(controller.getCartiOrdonateDinAnul("1998").size(), 1);
    }

    @Test
    public void TopDown_B() throws Exception {
        //adaugareCarte();
        //assertEquals(controller.getCarti().size(), 1);

        TopDown_A();

        cautareDupaAutor();
        assertEquals(controller.cautaCarte("Ceausescu").size(), 0);

        //cautareDupaAn();
        //assertEquals(controller.getCartiOrdonateDinAnul("1998").size(), 1);
    }


    @Test
    public void TopDown_C() throws Exception {
        //adaugareCarte();
        //assertEquals(controller.getCarti().size(), 1);

        //cautareDupaAutor();
        //assertEquals(controller.cautaCarte("Ceausescu").size(), 0);

        TopDown_B();

        cautareDupaAn();
        assertEquals(controller.getCartiOrdonateDinAnul("1998").size(), 1);

    }

}