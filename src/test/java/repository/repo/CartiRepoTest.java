package repository.repo;
import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.*;


public class CartiRepoTest {

    private static CartiRepoInterface cartiRepo;
    private static BibliotecaCtrl controller;

    @Before
    public void initializer() {
        cartiRepo = new CartiRepo();
        controller = new BibliotecaCtrl(cartiRepo);
    }

    @Test
    public void test_1() throws Exception {
        assertTrue(controller.cautaCarte("Andrei").size() == 0);
    }

    @Test
    public void test_2() throws Exception {
        Carte carte = new Carte();
        carte.setAnAparitie("1982");
        carte.setTitlu("titlu");
        carte.setCuvinteCheie(Collections.singletonList("cuvinte"));
        cartiRepo.adaugaCarte(carte);
        assertEquals(controller.cautaCarte("Andrei").size(), 0);
    }

    @Test
    public void test_3() throws Exception {
        Carte carte = new Carte();
        carte.setAnAparitie("1956");
        carte.setTitlu("titlu");
        carte.setReferenti(Collections.singletonList("Alex"));
        carte.setCuvinteCheie(Collections.singletonList("cuv"));
        cartiRepo.adaugaCarte(carte);
        assertEquals(controller.cautaCarte("Ilievici").size(), 0);
    }

    @Test
    public void test_4() throws Exception {
        Carte carte = new Carte();
        carte.setCuvinteCheie(Collections.singletonList("cheie"));
        carte.setReferenti(new ArrayList<String>(Collections.singleton("Ioan")));
        carte.setTitlu("Titlu");
        carte.setAnAparitie("1644");
        cartiRepo.adaugaCarte(carte);
        assertEquals(controller.cautaCarte("Ioan").size() , 0);
    }
}
